# class Dictionary
#
#   attr_accessor :entries
#
#   def initialize
#     @entries = {}
#   end
#
#   def add(entry)
#     if entry.is_a?(Hash)
#       @entries[entry.keys.first] = entry.values.first
#     else
#       @entries[entry] = nil
#     end
#   end
#
#   def keywords
#     @entries.keys.sort
#   end
#
#   def include?(entry)
#     self.keywords.include?(entry)
#   end
#
#   def find(search_term)
#     hits = {}
#     @entries.each do |key,value|
#       if key.include?(search_term)
#         hits[key] = value
#       end
#     end
#     # if self.include?(search_term)
#     #   hits[entry] = @entries[entry]
#     # end
#     return hits
#   end
#
#   def printable
#     string_arr = []
#     self.keywords.each do |key|
#       string_arr << %Q([#{key}] "#{@entries[key]}")
#     end
#
#     return string_arr.join("\n")
#
#
#     # @entries.each do |key,value|
#     #   puts "\[#{key}\] '#{value}'"
#     # end
#   end
# end




class Dictionary
  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add(entry)
    if entry.is_a?(Hash)
      @entries.merge!(entry)
    else
      @entries[entry] = nil
    end
  end

  def keywords
    @entries.keys.sort
  end

  def include?(keyword)
    self.keywords.include?(keyword)
  end

  def find(keyword)
    @entries.select do |key, value|
      key.include?(keyword)
    end
  end

  def printable
    keywords.map do |keyword|
      %Q{[#{keyword}] "#{@entries[keyword]}"}
    end.join("\n")
  end

end
