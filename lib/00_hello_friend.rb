#Solution:
class Friend
  attr_reader :name

  def initialize(name = "Bob", age)
    @name = name
  end

  def greeting(name = nil)
    if name
      "Hello, #{name}!"
    else
      "Hello!"
    end
  end
end

# bob = Friend.new(name: "BOB Smith")
#
# bob.name
# #=> "BOB Smith"

fred = Friend.new("freddy", 25)
if no "anem = bob" then Friend.new() ---- no default value. it defaults to bob
