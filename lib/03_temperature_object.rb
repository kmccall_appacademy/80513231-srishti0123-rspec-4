class Temperature
attr_accessor :fahrenheit, :celcius

  def initialize(options = {})
    @fahrenheit = options[:f] if options[:f]
    @celsius = options[:c] if options[:c]
  end

  def in_fahrenheit
    return @fahrenheit if @fahrenheit
    (@celsius * 9 / 5.0) + 32
  end

  def in_celsius
    return @celsius if @celsius
    (@fahrenheit - 32) * (5 / 9.0)
  end

  def self.from_celsius(temp)
    Temperature.new(:c => temp)
  end

  def self.from_fahrenheit(temp)
    Temperature.new(:f => temp)
  end
end

  class Celsius < Temperature
    def initialize(temp)
      @celsius = temp
    end
  end

  class Fahrenheit < Temperature
    def initialize(temp)
      @fahrenheit = temp
    end
  end
