class Timer
  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end

  def format_time(num)
    if num < 10
      "0#{num}"
    else
      "#{num}"
    end
  end

  def time_string
    hour = format_time(seconds / 3600)
    minute = format_time((seconds % 3600) / 60)
    second = format_time(seconds % 60)
    hour + ":" + minute + ":" + second
  end
end

































class Timer
  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end

end
